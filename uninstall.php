<?php

    /**
    * Uninstall methods
    * https://developer.wordpress.org/plugins/plugin-basics/uninstall-methods/
    */
   
    /**
    * Tasks
    * 
    * Remove database tables as necessary
    */
   
    if (!defined('WP_UNINSTALL_PLUGIN')) {
        die;
    }