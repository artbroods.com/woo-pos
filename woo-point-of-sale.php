<?php
    /**
     * Plugin Name: WooCommerce Point of Sale by ABTech Solutions
     * Plugin URI:  https://abtechsolutions.ca/
     * Description: An advanced toolkit for placing WooCommerce orders beautifully through a Point of Sale interface. Requires <a href="http://wordpress.org/plugins/woocommerce/">WooCommerce</a>.
     * Version:     1.0.0.0
     * Author:      ABTech Solutions
     * Author URI:  https://abtechsolutions.ca/
     * Text Domain: woo_point_of_sale
     *
     * @package     WOO-Point-Of-Sale
	 * @author      ABTech Solutions
	 * @category    Plugin
	 * @license     http://www.gnu.org/licenses/gpl-3.0.html GNU General Public License v3.0
	 *
	 * WC tested up to: 3.7.0
     */
    
    if ( ! defined( 'ABSPATH' ) ) {
        exit; // Exit if accessed directly
    }